function createCard(name, description, pictureUrl) {
  return `
    <div class="card shadow p-3 mb-5 bg-body rounded ">
      <img src="${pictureUrl}" class="card-img-top">
      <div class="card-body">
        <h5 class="card-title">${name}</h5>
        <p class="card-text">${description}</p>
      </div>
    </div>
  `;
}

window.addEventListener("DOMContentLoaded", async () => {
  const url = "http://localhost:8000/api/conferences/";

  try {
    const response = await fetch(url);

    if (!response.ok) {
      throw new Error("Response not okay");
    } else {
      const data = await response.json();

      let counter = 0;
      for (let conference of data.conferences) {
        const detailUrl = `http://localhost:8000${conference.href}`;
        const detailResponse = await fetch(detailUrl);
        if (detailResponse.ok) {
          const details = await detailResponse.json();
          const name = details.conference.name;
          const description = details.conference.description;
          const pictureUrl = details.conference.location.picture_url;
          const html = createCard(name, description, pictureUrl);
          const columns = document.querySelectorAll(".col");
          const column = columns[counter];
          column.innerHTML += html;
          counter++;
          if (counter === 3) {
            counter = 0;
          }
        }
      }
    }
  } catch (error) {
    console.error("error", error);
  }
});
